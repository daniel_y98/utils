#!/usr/bin/env python3

#
# Simple one time pad implementation
#

import sys

HELP = '''
Simple one time pad implementation (XOR).
Usage: onetimepad.py input pad [-a, -h, -a]
    -a: input and pad are in hex format separated by spaces (0x1f 0xa ...)
    -p: input is in hex format separated by spaces but pad is a normal string
    -v: verbose (PRINTS PAD)
    -l: human readable output
    -h: help
'''

# Not used
def onetimepad(message, pad):
    '''
    Performs one time pad algorithm on the given message and a pad
    message: str
    pad: str
    '''

    enc = ""

    if (len(message) > len(pad)):
        # Return empty string if message and pad length don't match
        return enc

    for i in range(0, len(message)):
        enc += chr(ord(message[i]) ^ ord(pad[i]))

    return enc

def onetimepad_hex(message, pad):
    '''
    Performs one time pad algorithm on the given message and pad
    message: hex string separated by spaces
    pad: hex string seperated by spaces

    Return encrypted string in hex values separated by spaces
    '''

    enc = ''

    message_list = message.split(' ')
    pad_list = pad.split(' ')

    if (len(pad_list) < len(message_list)):
        # Return empty string if message and pad length don't match
        return enc

    for i in range(0, len(message_list)):
        enc += hex(int(message_list[i], 16) ^ int(pad_list[i], 16))[2:] + ' '

    return enc[:len(enc) - 1]

def str_to_hex_str(string):
    hex_str = ''

    for s in string:
        hex_str += hex(ord(s))[2:] + ' '

    return hex_str[:len(hex_str) - 1]

def hex_str_to_str(hex_string):
    string = ''

    for h in hex_string.split(' '):
        string += chr(int(h, 16))

    return string

if __name__ == '__main__':
    if '-h' in sys.argv or len(sys.argv[1]) == 0 or len(sys.argv[2]) == 0:
        print(HELP)
        exit()

    message = sys.argv[1]
    pad = sys.argv[2]

    if '-v' in sys.argv:
        print("PAD LENGTH: " + str(len(pad)))
        print("MESSAGE LENGTH: " + str(len(message)))

    if '-a' not in sys.argv and '-p' not in sys.argv:
        pad = str_to_hex_str(pad)
        message = str_to_hex_str(message)

    if '-p' in sys.argv:
        pad = str_to_hex_str(pad)

    if '-v' in sys.argv:
        print("PAD: " + pad)
        print("MESSAGE: " + message)

    result = onetimepad_hex(message, pad)

    print('------------------------------------------------------')
    if ('-l' in sys.argv):
        print(hex_str_to_str(result))
    else:
        print (result)
    print('------------------------------------------------------')
